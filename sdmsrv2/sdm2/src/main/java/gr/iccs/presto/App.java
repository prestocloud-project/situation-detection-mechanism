package gr.iccs.presto;

import java.io.InputStream;
import java.io.StringWriter;
import java.security.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.wso2.siddhi.core.SiddhiAppRuntime;
import org.wso2.siddhi.core.SiddhiManager;
import org.wso2.siddhi.core.event.Event;
import org.wso2.siddhi.core.stream.input.InputHandler;
import org.wso2.siddhi.core.stream.output.StreamCallback;
import org.apache.commons.io.IOUtils;

public class App {

	public static void main(String[] args) throws InterruptedException {
		String siddhiApp = "";

		try {
			ClassLoader loader = Thread.currentThread().getContextClassLoader();

			InputStream appStream = loader.getResourceAsStream("RULES.siddhi");
			System.out.println("Created inputstream from : RULES.siddhi");
			StringWriter wr = new StringWriter();
			IOUtils.copy(appStream, wr, "UTF-8");
			System.out.println("copy ok");
			siddhiApp = wr.toString();
			System.out.println("LOADED RULES.siddhi .");

		} catch (Throwable t) {
			System.out.println("FAILED TO LOAD RULES.siddhi !");
			t.printStackTrace();
		}

		SiddhiManager siddhiManager = new SiddhiManager();

		SiddhiAppRuntime siddhiAppRuntime = siddhiManager.createSiddhiAppRuntime(siddhiApp);
		siddhiAppRuntime.addCallback("outputStream", new StreamCallback() {
			public int eventCount = 0;
			public int timeSpent = 0;
			long startTime = System.currentTimeMillis();

			@Override
			public void receive(Event[] events) {
				for (Event event : events) {
					System.out.println("outputStream : " + event.toString());
					/*
					 * eventCount++; timeSpent += (System.currentTimeMillis() - (Long)
					 * event.getData(3)); if (eventCount % 1000000 == 0) { System.out.println(
					 * "Throughput : " + (eventCount * 1000) / ((System.currentTimeMillis()) -
					 * startTime)); System.out.println("Time spent :  " + (timeSpent * 1.0 /
					 * eventCount)); startTime = System.currentTimeMillis(); eventCount = 0;
					 * timeSpent = 0; }
					 */
				}
			}
		});
		siddhiAppRuntime.addCallback("msgStream", new StreamCallback() {
			@Override
			public void receive(Event[] events) {
				for (Event event : events) {
					// System.out.println("msgStream : " + event.toString());
					long ts = event.getTimestamp();
					String edate = new SimpleDateFormat("HH:mm:ss").format(new Date(ts));
					String msg = event.getData(0).toString();
					System.out.println(edate + " : " + msg);
				}
			}
		});

		// InputHandler inputHandler =
		// siddhiAppRuntime.getInputHandler("cseEventStream");

		siddhiAppRuntime.start();

		/*
		 * while (true) { inputHandler.send(new Object[] { "WSO2", 55.6f, 100,
		 * System.currentTimeMillis() }); inputHandler.send(new Object[] { "IBM", 75.6f,
		 * 100, System.currentTimeMillis() }); inputHandler.send(new Object[] { "WSO2",
		 * 100f, 80, System.currentTimeMillis() }); inputHandler.send(new Object[] {
		 * "IBM", 75.6f, 100, System.currentTimeMillis() }); inputHandler.send(new
		 * Object[] { "WSO2", 55.6f, 100, System.currentTimeMillis() });
		 * inputHandler.send(new Object[] { "IBM", 75.6f, 100,
		 * System.currentTimeMillis() }); inputHandler.send(new Object[] { "WSO2", 100f,
		 * 80, System.currentTimeMillis() }); inputHandler.send(new Object[] { "IBM",
		 * 75.6f, 100, System.currentTimeMillis() }); }
		 */

	}
}
