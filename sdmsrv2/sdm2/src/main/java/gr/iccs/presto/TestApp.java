package gr.iccs.presto;

import org.wso2.siddhi.core.SiddhiAppRuntime;
import org.wso2.siddhi.core.SiddhiManager;
import org.wso2.siddhi.core.event.Event;
import org.wso2.siddhi.core.stream.input.InputHandler;
import org.wso2.siddhi.core.stream.output.StreamCallback;

public class TestApp {

    public static void main(String[] args) throws InterruptedException {
            SiddhiManager siddhiManager = new SiddhiManager();

            String siddhiApp = "" +
                    "define stream cseEventStream (symbol string, price float, volume long, timestamp long);" +
                    "" +
                    "@info(name = 'query1') " +
                    "from cseEventStream[700 > price] " +
                    "select * " +
                    "insert into outputStream ;";

            SiddhiAppRuntime siddhiAppRuntime = siddhiManager.createSiddhiAppRuntime(siddhiApp);
            siddhiAppRuntime.addCallback("outputStream", new StreamCallback() {
                public int eventCount = 0;
            	public int timeSpent = 0;
                long startTime = System.currentTimeMillis();

            	@Override
                public void receive(Event[] events) {
                for (Event event : events) {
                    eventCount++;
                        timeSpent += (System.currentTimeMillis() - (Long) event.getData(3));
                    if (eventCount % 1000000 == 0) {
                            System.out.println("Throughput : " + (eventCount * 1000) / ((System.currentTimeMillis()) -
                                    startTime));
                            System.out.println("Time spent :  " + (timeSpent * 1.0 / eventCount));
                            startTime = System.currentTimeMillis();
                            eventCount = 0;
                            timeSpent = 0;
                        }
                    }
            }
            });


            InputHandler inputHandler = siddhiAppRuntime.getInputHandler("cseEventStream");
            siddhiAppRuntime.start();
            while (true) {
                inputHandler.send(new Object[]{"WSO2", 55.6f, 100, System.currentTimeMillis()});
            inputHandler.send(new Object[]{"IBM", 75.6f, 100, System.currentTimeMillis()});
                inputHandler.send(new Object[]{"WSO2", 100f, 80, System.currentTimeMillis()});
            inputHandler.send(new Object[]{"IBM", 75.6f, 100, System.currentTimeMillis()});
                inputHandler.send(new Object[]{"WSO2", 55.6f, 100, System.currentTimeMillis()});
            inputHandler.send(new Object[]{"IBM", 75.6f, 100, System.currentTimeMillis()});
                inputHandler.send(new Object[]{"WSO2", 100f, 80, System.currentTimeMillis()});
            inputHandler.send(new Object[]{"IBM", 75.6f, 100, System.currentTimeMillis()});
            }

        }
}
