package gr.iccs.presto;

import org.kie.api.KieBaseConfiguration;
import org.kie.api.KieServices;
import org.kie.api.conf.EventProcessingOption;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import java.io.*;
import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeoutException;
import java.nio.charset.*;
import java.text.NumberFormat;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.rabbitmq.client.*;

import gr.iccs.presto.SituationPublisher;

public class App {

	private static final String DEFAULT_EXCHANGE_NAME = "LOGSTASH_TO_SDM";
	private static final String DEFAULT_IN_TOPIC = "mobile.preproc.out"; // is it the topic ?

	public static final void main(String[] args) {
		try {
			printSystemStatus();
			// load up the knowledge base
			KieServices ks = KieServices.Factory.get();
			KieContainer kContainer = ks.getKieClasspathContainer();
			final KieSession kSession = kContainer.newKieSession("ksession-rules");

			ClassLoader loader = Thread.currentThread().getContextClassLoader();
			Properties props = new Properties();
			try (InputStream resourceStream = loader.getResourceAsStream("output.properties")) {
				props.load(resourceStream);
			}
			String spHost = props.getProperty("host", "127.0.0.1");
			String spUser = props.getProperty("username", "rmquser");
			String spPass = props.getProperty("password", "rmqpass");
			String spExchange = props.getProperty("exchange", "SDM-SITUATIONS");

			kSession.setGlobal("sp", new SituationPublisher(spHost, spUser, spPass, spExchange));

			// Use with instead of fireAll
			ExecutorService thread = Executors.newSingleThreadExecutor();
			final Future fireUntilHaltResult = thread.submit(new Runnable() {
				@Override
				public void run() {
					while (true) {
						System.out.println("^^^^ RULE ENGINE (RE)START @" + new Date());
						try {
							kSession.fireUntilHalt();
						} catch (Exception e) {
							System.out.println("fireUntilHalt EXCEPTION !!! " + e.getMessage().toString());
							// e.printStackTrace();
						}
					}
				}
			});
			App app = new App();
			app.startEventLoader(kSession);

		} catch (Throwable t) {
			t.printStackTrace();
		}
	}

	private static void printSystemStatus() {
		Runtime runtime = Runtime.getRuntime();

		final NumberFormat format = NumberFormat.getInstance();

		final long maxMem = runtime.maxMemory();
		final long allocMem = runtime.totalMemory();
		final long freeMem = runtime.freeMemory();
		final long ONE_MB = 1024 * 1024;
		final String mega = " MB";

		System.out.println("============ JVM Memory Info ==============");
		System.out.println("Free: " + format.format(freeMem / ONE_MB) + mega);
		System.out.println("Allocated: " + format.format(allocMem / ONE_MB) + mega);
		System.out.println("Max: " + format.format(maxMem / ONE_MB) + mega);
		System.out.println("Total free: " + format.format((freeMem + (maxMem - allocMem)) / ONE_MB) + mega);
		System.out.println("============================================\n");
		System.out.println("============ JVM Arguments  ==============");
		RuntimeMXBean runtimeMXBean = ManagementFactory.getRuntimeMXBean();
		List<String> jvmArgs = runtimeMXBean.getInputArguments();
		for (String arg : jvmArgs) {
		    System.out.println(arg);
		}
		System.out.println("============================================\n");

	}

	public static String convertStreamToString(InputStream inputStream, Charset charset) throws IOException {

		try (Scanner scanner = new Scanner(inputStream, charset.name())) {
			return scanner.useDelimiter("\\A").next();
		}
	}

	private void testWithJsonFile(KieSession kSession) throws IOException {
		// Load a test json
		ClassLoader classloader = Thread.currentThread().getContextClassLoader();
		InputStream is = classloader.getResourceAsStream("test-android.json");
		String jsonStr = convertStreamToString(is, StandardCharsets.UTF_8);
		System.out.println("========= jsonStr ========= **");
		System.out.println(jsonStr);

		System.out.println("=========| gson from java |=========");
		Gson gson = new GsonBuilder().create();
		// gson.toJson(jsonStr, System.out);
		JsonObject json = gson.fromJson(jsonStr, JsonObject.class);

		// go !
		kSession.insert(json);
		kSession.fireAllRules();
	}

	private void startEventLoader(KieSession kSession) throws IOException, TimeoutException {
		ClassLoader loader = Thread.currentThread().getContextClassLoader();
		Properties props = new Properties();
		try (InputStream resourceStream = loader.getResourceAsStream("input.properties")) {
			props.load(resourceStream);
		}
		String inHost = props.getProperty("host", "127.0.0.1");
		String inUser = props.getProperty("username", "rmquser");
		String inPass = props.getProperty("password", "rmqpass");
		String inExchange = props.getProperty("exchange", DEFAULT_EXCHANGE_NAME);
		String inTopic = props.getProperty("topic", DEFAULT_IN_TOPIC);

		ConnectionFactory factory = new ConnectionFactory();
		factory.setUsername(inUser);
		factory.setPassword(inPass);
		factory.setHost(inHost);
		Connection connection = factory.newConnection();
		Channel channel = connection.createChannel();

		channel.exchangeDeclare(inExchange, "topic");
		String queueName = channel.queueDeclare().getQueue();

		channel.queueBind(queueName, inExchange, inTopic);

		System.out.println(
				" [*] Waiting for AMQP messages. Exchange=" + inExchange + " Topic=" + inTopic + " host=" + inHost);

		EventLoader consumer = new EventLoader(channel, kSession);

		channel.basicConsume(queueName, true, consumer);
	}

}
