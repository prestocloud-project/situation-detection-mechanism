package gr.iccs.presto;

import java.io.IOException;

import org.kie.api.runtime.KieSession;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;

public class EventLoader extends DefaultConsumer {

	private KieSession _kSession;
	private Gson _gson;

	public EventLoader(Channel channel, KieSession kSession) {
		super(channel);
		this._kSession = kSession;
		this._gson = new GsonBuilder().create();				
	}
	
	@Override
    public void handleDelivery(String consumerTag, Envelope envelope,
                               AMQP.BasicProperties properties, byte[] body) throws IOException {
      String message = new String(body, "UTF-8");
      //System.out.println(" [x] AMQP EVENT FROM TOPIC '" + envelope.getRoutingKey() + "'\n:'" + message + "'");
      //System.out.println(" [x] AMQP EVENT FROM TOPIC '" + envelope.getRoutingKey() );
      if (this._kSession != null) {
      	//insert event into Drools Fusion (CEP)
    	JsonObject json = this._gson.fromJson(message, JsonObject.class);
    	//System.out.println(json);
  		// go !
    	
  		//try {
			this._kSession.getEntryPoint("INPUT").insert(json);
			long facts = this._kSession.getFactCount();
			//System.out.println(" [x] FACTS in KB="+facts);
			//this._kSession.fireAllRules(); //comment out if fireUntilHalt mode is used
		//} catch (Exception e) {
		//	System.out.println("Error processing message :" + json);
		//	e.printStackTrace();
		//}
      	
      }
    }
}
