package gr.iccs.presto;

import com.rabbitmq.client.*;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class SituationPublisher {

	private String EXCHANGE_NAME = "SDM-SITUATIONS";
	private Channel channel;
	private Connection connection;

	public SituationPublisher(String host, String uname, String pass, String exchangeName) throws IOException, TimeoutException {
		super();
		this.EXCHANGE_NAME = exchangeName;
		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost(host);
		factory.setPassword(pass);
		factory.setUsername(uname);
		this.connection = factory.newConnection();
		this.channel = connection.createChannel();
		this.channel.exchangeDeclare(EXCHANGE_NAME, "topic");
	}

	public void publish(String topic, String message) throws Exception {

		String routingKey = topic;
		channel.basicPublish(EXCHANGE_NAME, routingKey, null, message.getBytes());
		System.out.println(" [x] AMQP Sent '" + routingKey + "':'" + message + "'");
	}

}
