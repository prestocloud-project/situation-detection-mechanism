#!/usr/bin/env python
import pika
import sys

#EXCHANGE = "SDM-SITUATIONS"
EXCHANGE = "SDM2-SITUATIONS"
credentials = pika.PlainCredentials('rabbitmq', 'rabbitmq')
connection = pika.BlockingConnection(pika.ConnectionParameters(
    host='localhost',credentials=credentials))

channel = connection.channel()

channel.exchange_declare(exchange=EXCHANGE, exchange_type='topic')

result = channel.queue_declare(exclusive=True)
queue_name = result.method.queue

binding_keys = sys.argv[1:]
if not binding_keys:
  sys.stderr.write("Usage: %s [topic]... ('#' for all keys/topics)\n" % sys.argv[0])
  sys.exit(1)
print('Searching topics on exchange ' + EXCHANGE)
for binding_key in binding_keys:
  print(' found topic : ' + binding_key )
  channel.queue_bind(exchange=EXCHANGE, queue=queue_name, routing_key=binding_key)

print(' [*] Waiting for logs. To exit press CTRL+C')

def callback(ch, method, properties, body):
   print(" [x] %r:%r" % (method.routing_key, body))

channel.basic_consume(callback, queue=queue_name, no_ack=True)
channel.start_consuming()
